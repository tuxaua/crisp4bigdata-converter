name := "crisp4bigdata-converter"

version := "1.0"

scalaVersion := "2.11.11"

val kafkaVersion = "0.11.0.1"

libraryDependencies ++= {
  Seq(
    "com.typesafe.scala-logging"   %% "scala-logging-slf4j" % "2.1.2",
    "org.apache.kafka" % "connect-api" % kafkaVersion % "provided",
    "org.scalatest" %% "scalatest" % "3.0.4" % "test"
  )
}

resolvers ++= Seq(
  Resolver.sonatypeRepo("public"),
  "Confluent Maven Repo" at "http://packages.confluent.io/maven/"
)
