package com.eneco.trading.kafka.connect.ftp.source

import org.apache.kafka.common.Configurable
import org.apache.kafka.connect.source.SourceRecord

trait SourceRecordConverter extends Configurable {
  def convert(in:SourceRecord) : java.util.List[SourceRecord]
}
