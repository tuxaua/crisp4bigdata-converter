package de.feu.lgmmia.crisp4bigdata.converter

import java.util

import scala.collection.JavaConverters.seqAsJavaListConverter

import org.apache.kafka.connect.source.SourceRecord

import com.eneco.trading.kafka.connect.ftp.source.SourceRecordConverter
import com.typesafe.scalalogging.slf4j.StrictLogging

class MetaplatS2Converter extends SourceRecordConverter with StrictLogging {

  val separator = ";"

  override def configure(props: util.Map[String, _]): Unit = {}

  override def convert(in: SourceRecord): util.List[SourceRecord] = {
    var validRows = 0
    var incompleteRows = 0
    var wronglyFormattedRows = 0
    val value = new String(in.value.asInstanceOf[Array[Byte]])
    val records = value.split("\n").flatMap { row =>
      try {
        val columns = row.split(separator)
        val data = MetaplatS2Data(columns(0), columns(1), columns(2).toDouble, columns(3).toDouble,
                                  columns(4).toDouble, columns(5).toDouble, columns(6).toDouble,
                                  columns(7).toDouble, columns(8).toDouble, columns(9).toDouble)
        validRows += 1
        Option(new SourceRecord(in.sourcePartition, in.sourceOffset, in.topic, 0, data.schema, data.content))
      } catch {
        case _: IndexOutOfBoundsException =>
          incompleteRows += 1
          None
        case _: NumberFormatException =>
          wronglyFormattedRows += 1
          None
      }
    }
    logger.info(s"Completed source record conversion of ${value.length} characters: ${validRows} valid rows, ${incompleteRows} incomplete, ${wronglyFormattedRows} wrongly formatted")  
    records.toList.asJava
  }

}
