package de.feu.lgmmia.crisp4bigdata.converter

import org.apache.kafka.connect.data.Struct
import org.apache.kafka.connect.source.SourceRecord
import org.scalatest.{ Matchers, WordSpec }

import scala.collection.JavaConverters._

class MetaplatS2ConverterSpec extends WordSpec with Matchers {

  val sourcePartition = Map("0" -> 0L).asJava
  val sourceOffset = Map("filename" -> 0L).asJava

  "A line" when {

    "representing a valid S2 record" should {

      val input = "K01980;23S ribosomal RNA;11.79648646;17.54756407;16.55812848;7.070375553;9.904088434;10.33047681;11.34235283;12.84408234"
      val inputSourceRecord = new SourceRecord(sourcePartition, sourceOffset, "topic", 0, null, input.getBytes)
      val convertedSourceRecords = new MetaplatS2Converter().convert(inputSourceRecord)

      "yield one SourceRecord" in {
        convertedSourceRecords.size shouldBe 1
      }

      "yield a SourceRecord with all input values" in {
        val structure = convertedSourceRecords.get(0).value.asInstanceOf[Struct]
        structure.getString("keggOrthologue") shouldBe "K01980"
        structure.getString("microbialGene") shouldBe "23S ribosomal RNA"
        structure.getFloat64("relativeAbundance2019N00001") shouldBe 11.79648646
        structure.getFloat64("relativeAbundance2019N00002") shouldBe 17.54756407
        structure.getFloat64("relativeAbundance2019N00003") shouldBe 16.55812848
        structure.getFloat64("relativeAbundance2019N00004") shouldBe 7.070375553
        structure.getFloat64("relativeAbundance2019N00005") shouldBe 9.904088434
        structure.getFloat64("relativeAbundance2019N00006") shouldBe 10.33047681
        structure.getFloat64("relativeAbundance2019N00007") shouldBe 11.34235283
        structure.getFloat64("relativeAbundance2019N00008") shouldBe 12.84408234
      }
    }

    "not containing the right number of columns" should {
      val input = "K01980;23S ribosomal RNA;11.79648646;17.54756407;16.55812848"
      val inputSourceRecord = new SourceRecord(sourcePartition, sourceOffset, "topic", 0, null, input.getBytes)
      val convertedSourceRecords = new MetaplatS2Converter().convert(inputSourceRecord)

      "be skipped" in {
        convertedSourceRecords.size shouldBe 0
      }
    }

    "containing an invalid floating point number" should {
      val input = "K01980;23S ribosomal RNA;11.79648646;17.54756407;16.55812848;7.070375553;9.904088434;10.33047681;11.34235283;12.XXX"
      val inputSourceRecord = new SourceRecord(sourcePartition, sourceOffset, "topic", 0, null, input.getBytes)
      val convertedSourceRecords = new MetaplatS2Converter().convert(inputSourceRecord)

      "be skipped" in {
        convertedSourceRecords.size shouldBe 0
      }
    }
  }

  "An empty line" should {
    val input = ""
    val inputSourceRecord = new SourceRecord(sourcePartition, sourceOffset, "topic", 0, null, input.getBytes)
    val convertedSourceRecords = new MetaplatS2Converter().convert(inputSourceRecord)

    "be skipped" in {
      convertedSourceRecords.size shouldBe 0
    }
  }

  "A set of several lines containing valid S2 records" should {

    val input = """
      |01980;23S ribosomal RNA;11.79648646;17.54756407;16.55812848;7.070375553;9.904088434;10.33047681;11.34235283;12.84408234
      |K01977;16S ribosomal RNA;7.433653271;11.21805284;10.47502621;4.555449801;6.223779066;6.547853101;7.12047883;8.056722
      |K03046;DNA-directed RNA polymerase subunit beta' [EC:2.7.7.6];1.279373326;1.843958244;1.291530419;0.91598316;1.444456949;1.370082994;1.471863596;1.274232464
    """.stripMargin

    "yield as many records" in {
      val inputSourceRecord = new SourceRecord(sourcePartition, sourceOffset, "topic", 0, null, input.getBytes)
      val convertedSourceRecords = new MetaplatS2Converter().convert(inputSourceRecord)
      convertedSourceRecords.size shouldBe 3
    }
  }

}
